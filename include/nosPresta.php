<h1 class="h1nosPresta">Découvrez toutes nos préstations</h1>

<section class="section-nosPresta">
    <article class="wrapper-presta prestation-list">
        <?php
        // ON LANCE LA REQUETE PUIS JE BOUCLE WHILE POUR AFFICHÉ TOUTE LES PRESTATION //
        $req = $cnx->query('SELECT * FROM prestation');
        while ($data = $req->fetch()) {
            echo "<div class='wrapper-2-presta prestation'>";
            echo "<h2 class='title-presta reveal'>" . $data['title'] . "</h2>";
            echo "<img class='img-prestation reveal' src='./assets/upload/imgPrestations/" . $data['imgname'] . "' width='300px' >";
            echo "<p class='reveal'>" . $data['description'] . "</p>";
            echo '<a class="reveal" href="?page=prestations&id=' . $data['id'] . '"">Voir plus +</a>';
            echo "</div>";
        }
        ?>
    </article>
</section>