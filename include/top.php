<?php
// OUVERTURE DES SESSION POUR RECUPERER LES IDENTIFAINTS ADMIN (ZONE ADMINISTRATEUR) //
session_start();
// J'INCLUS LA CONNEXION A MA BDD //
require_once('cnxBdd.php');
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Voici le site vitrine de l'entreprise Hexamanut qui propose des services de manutention pour professionel du BTP ainsi que pour des particuliers.
    Le site est réaliser dans le but de pouvoir montrer les compétances et les qualités de l'entreprise Hexamanut.Entreprise manutention BTP basé sur Lyon et sa région.">
    <link rel="stylesheet" href="./stylesheet/main.css">
    <link rel="stylesheet" href="./assets/icons/icofont.min.css">
    <title>Hexamanut</title>
</head>