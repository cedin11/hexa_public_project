<?php
// JE CRÉE MA REQUETE TABLE PRESTATION EN FONCTION DE L'ID CORRESPONDANT //
$req = $cnx->prepare("SELECT * FROM prestation WHERE id=?");
$req->execute([$_GET['id']]);
$data = $req->fetch();

// JE CIBLE MON DOSSIER EN FONCTION DE MON ID PRESTATION AVEC MA VARIABLE $DATA //
$dir = "../assets/upload/imgPrestations/imgSupplementaires/" . $data['id'];
// ARRAY_SLICE :  PERMET DE SUPPRIMÉ LE . ET LA .. DU TABLEAU SCANDIR //
$a = array_slice(scandir($dir), 2);
// J'AFFICHE LE CONTENU SOUHAITER //
echo '<div class="content">';
echo '<div class="container-seepresta">';
echo "<img class='img-seepresta' src='../assets/upload/imgPrestations/" . $data['imgname'] . " ' >";
echo '<article class="contenu-seepresta">';
echo $data['contenu'];
echo '</article>';
echo '</div>';
echo '</div>';
echo '<section class="galerie-seepresta">';
// ICI JE BOUCLE MES IMAGES DU DOSSIER SELECTIONNÉ //
foreach ($a as $img) :
    echo '<div class="wrap-seepresta">';
    echo '<img class="dir-seepresta" src="' . $dir . '/' . $img . '">';
    echo '</div>';
endforeach;
echo '</section>';
