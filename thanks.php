<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./stylesheet/thanks.css">
    <title>message envoyé</title>
</head>

<body>
    <img class="img-thanks" src="./assets/image/logo.png" alt="">
    <div class="content-thanks">
        <h1 class="h1-thanks">Hexamanut vous remercie! Votre message a bien été envoyé. Nous vous contacterons dans les plus bref delais.</h1>
        <div class="container-btn">
            <a class="button-thanks" href="./index.php">Retour a l'accueil</a>
        </div>
    </div>
</body>

</html>