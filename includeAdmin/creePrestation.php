<h1 class="h1">Créer votre préstation</h1>
<?php
// JE CRÉE UN SWITCH AVEC POUR DEFAUT LE FORMULAIRE //
switch ($_GET['action']) {
    default:
?>
        <div class="container-form">
            <form action="index.php?page=creePrestation&action=save" method="POST" enctype="multipart/form-data">
                <input class="input-form" placeholder="Nom préstation *" required="required" type="text" name="title" id="title"><br>
                <input class="input-form" placeholder="Description préstation *" required="required" type="text" name="description" id="description"><br>
                <label for="file">Image principal *: (Taille Max = 150ko)</label><br>
                <input type="file" name="file"><br>
                <textarea name="contenu" id="contenu"></textarea><br>
                <label for="files">Images supplémentaires *:(Taille Max par img = 80ko)</label><br>
                <input type="file" name="files[]" multiple /><br>
                <div class="container-btn">
                    <button type="submit">Envoyer</button>
                </div>
            </form>
        </div>
<?php
        break;

    case 'save':
        if (isset($_FILES['file']) && ($_FILES['files'])) {

            // JE RECUPERE DANS DES VARIABLES LES VALEURS DE MON IMAGE PRINCIPAL //
            $tmpName = $_FILES['file']['tmp_name'];
            $name = $_FILES['file']['name'];
            $size = $_FILES['file']['size'];
            $error = $_FILES['file']['error'];


            // IDEM MAIS POUR LES IMAGES SUPPLEMENTAIRES //
            $tmpNameS = $_FILES['files']['tmp_name'];
            $nameS = $_FILES['files']['name'];
            $sizeS = $_FILES['files']['size'];
            $errorS = $_FILES['files']['error'];

            // ON COMPTE LE NOMBRE DE FICHIER ENVOYÉ //
            $nbfichiersEnvoyes = count($tmpNameS);
            // JE SÉPARE LE NOM DE L'EXTENSION //
            $tabExtension = explode('.', $name);
            // JE PASSE LE TOUT EN MINUSCULE //
            $extension = strtolower(end($tabExtension));
            // JE CRÉÉ UNE VARIABLE AVEC UNE TAILLE MAX POUR LES IMAGES
            $maxSize = 600000;
            // VARIABLE AVEC TABLEAU DES EXTENSIONS AUTORISER //
            $extensions = ['jpg', 'png', 'jpeg', 'webp'];
            // JE CRÉE UNE CONDITION QUI VERIFIE LES EXTENSIONS ET LA TAILLE MAX DES IMAGES //
            if (in_array($extension, $extensions) && $size <= $maxSize && $error == 0) {
                // JE GENERE UN UNOIQUE ID QUI RESSEMBLERA A CA : 5f586bf96dcd38.73540086 //
                $uniqueName = uniqid('', true);
                // PUIS JE CONCQUATENE AVEC L'EXTENSION //
                //$file = 5f586bf96dcd38.73540086.WEBP //
                $file = $uniqueName . "." . 'webp';
                // J'ENVOIE MON IMAGE DANS LE DOSSIER AVEC  move_uploaded_file //
                move_uploaded_file($tmpName, '../assets/upload/imgPrestations/' . $file);
                // J'EXECUTE MA REQUETE POUR INSERTIONS //
                $ins = $cnx->prepare("INSERT INTO prestation SET imgname=?, title=?, description=?, dateCrea=?,contenu=?");
                $ins->execute([$file, $_POST['title'], $_POST['description'], date("Y-m-d"), $_POST['contenu']]);
                // JE LANCE UNE REQUETE OU POUR RECUPERER LA MAX ID DE MES PRESTATIONS DONC LE DERNIER CRÉE //
                $req = $cnx->query("SELECT MAX(id) FROM prestation");
                $data = $req->fetch();
                // PUIS DE JE CRÉE UN DOSSIER DANS LES UPLOAD AVEC POUR NOM DE DOSSIER , L'ID CORRESPONDANT A MON DERNIER PRESTATION //
                mkdir('../assets/upload/imgPrestations/imgSupplementaires/' . $data[0]);
                // JE BOUCLE POUR INSERER DANS LE DOSSIER CORRESPONDANT LES IMAGES SUPPLEMENTAIRES //
                for ($i = 0; $i < $nbfichiersEnvoyes; $i++) {
                    move_uploaded_file($tmpNameS[$i], '../assets/upload/imgPrestations/imgSupplementaires/' . $data[0] . '/' . $nameS[$i]);
                }
                // On scanne le dossier ciblé
                $scandir = scandir('../assets/upload/imgPrestations/imgSupplementaires/' . $data[0] . '/');
                foreach ($scandir as $fichier) {
                    // On récupère l'extension du fichier 
                    $extension = strtolower(pathinfo($fichier, PATHINFO_EXTENSION));
                    // Si l'extension est png ou jpg, on lance la conversion
                    if (($extension == 'png') || ($extension == 'jpg') || ($extension == 'jpeg')) {
                        // Si png, on récupère la méthode de conversion du fichier en png
                        if ($extension == 'png') {
                            $crea = 'imagecreatefrompng';
                        }
                        // Si jpg, on récupère la méthode de conversion du fichier en jpg
                        if ($extension == 'jpg') {
                            $crea = 'imagecreatefromjpeg';
                        }
                        // Si jpeg, on récupère la méthode de conversion du fichier en jpg
                        if ($extension == 'jpeg') {
                            $crea = 'imagecreatefromjpeg';
                        }
                        // On initialise le fichier dans son format d'origine
                        $im = $crea('../assets/upload/imgPrestations/imgSupplementaires/' . $data[0] . '/' . $fichier);
                        // On remplace son extension par webp
                        $newImagePath = str_replace($extension, "webp", '../assets/upload/imgPrestations/imgSupplementaires/' . $data[0] . '/' . $fichier);
                        // Je choisis le taux de conversion
                        $quality = 50;
                        // On convertit en webp
                        imagewebp($im, $newImagePath, $quality);
                        // On supprime le fichier
                        $sup = '../assets/upload/imgPrestations/imgSupplementaires/' . $data[0] . '/' . $fichier;
                        unlink($sup);
                    }
                }
            }
            // SI OK J'AFFICHE //
            echo '<h2 style="width:100% ;text-align:center;">Préstation enregistrée</h2>';
        } else {
            // SI ERREUR ALORS J'AFFICHE // 
            echo '<h2 style="width:100% ;text-align:center;">Une erreur est survenue</h2>';
        }
        break;
}
