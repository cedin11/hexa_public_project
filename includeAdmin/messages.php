<h1 class="h1">Liste des contacts</h1>
<div class="container-contact">
    <?php
    // ICI JE VEUX RECUPERER TOUT LES MESSAGES RECU DU FORMULAIRE DE CONTACT //
    // JE LANCE DONC UNE REQUETE //
    $req = $cnx->query('SELECT * FROM contact');
    // ET JE BOUCLE POUR AFFICHÉ TOUT LES MESSAGES //
    while ($data = $req->fetch()) {
        echo '<li class="li-contact">';
        echo '<h3>Nom :</h3>';
        echo "<p>" . $data['nom'] . "</p>";
        echo '<h3>Mail :</h3>';
        echo "<a href=''>" . $data['mail'] . "</a>";
        echo '<h3>Telephone :</h3>';
        echo "<p>" . $data['tel'] . "</p>";
        echo '<h3>Date :</h3>';
        echo "<p>" . $data['date'] . "</p>";
        echo '<h3>Sujet :</h3>';
        echo "<p>" . $data['sujet'] . "</p>";
        echo '<h3>Message :</h3>';
        echo "<p>" . $data['message'] . "</p>";
        echo '</li>';
    }
    ?>
</div>