<?php
// JE LANCE MON SWITCH  AVEC POUR DEFAUT LE FORMULAIRE DE CONTACT //
switch ($_GET['action']) {
    default:
?>
        <main class="content-form">
            <form action="index.php?page=contact&action=envoie" method="post" id="formulaire">

                <input class="input-contact" type="text" name="nom" placeholder="Nom et Prénom *" required="required"><br>
                <input class="input-contact" type="email" name="mail" placeholder="E-mail *" required="required"><br>
                <input class="input-contact" type="phone" name="tel" placeholder="Téléphone *" required="required"><br>
                <input class="input-contact" type="text" name="sujet" placeholder="Sujet *" required="required"><br>
                <textarea class="area-contact" name="message" id="message" placeholder="Votre demande *" required="required"></textarea><br>
                <!-- Ceci est une balise cacher qui doit avoir une class en display block. Elle permet de faire une premiere securiter pour les spams bot c'est ce que l'on appele un honeypot -->
                <label for="nickname" aria-hidden="true" class="user-cannot-see"> Nickname
                    <input styles="display:none" type="text" name="nickname" id="nickname" class="user-cannot-see" tabindex="-1" autocomplete="off">
                </label>
                <div class="container-btn">
                    <button class="contact-btn">Envoyer</button>
                </div>
            </form>
        </main>
<?php

        break;
        // A L'ENVOIE DU FORMULAIRE //
    case "envoie":
        // CONDITION QUI VERFIFIE LE CHAMP ANTI-BOT : S'IL EST REMPLI ALORS LE SCRIPT D'ENVOIE DE MAIL NE SERA PAS EXECUTER //
        $honeybot = filter_input(INPUT_POST, htmlspecialchars('nickname'));
        if ($honeybot) {
            header($_SERVER['SERVER_PROTOCOL'] . '405 Method Not Allowed');
            exit;
        }
        $req = "INSERT INTO contact (nom,sujet,message,mail,date,tel) VALUES (:nom, :sujet, :message,  :mail, :date, :tel)";
        $ins = $cnx->prepare($req);
        // INSERTION AVEC BINDPARAM POUR ENREGISTRER EN BDD //
        $ins->bindParam(':nom', $_POST['nom'], PDO::PARAM_STR);
        $ins->bindParam(':sujet', $_POST['sujet'], PDO::PARAM_STR);
        $ins->bindParam(':message', $_POST['message'], PDO::PARAM_STR);
        $ins->bindParam(':mail', $_POST['mail'], PDO::PARAM_STR);
        // POUR ENREGISTRER UN DATE OU DATETIME IL FAUT UTILISER BINDVALUE POUR ENREGISTRER UNE VALEUR //
        $ins->bindValue(':date', date("Y-m-d"), PDO::PARAM_STR);
        $ins->bindParam(':tel', $_POST['tel'], PDO::PARAM_STR);
        // ON EXECUTE L'INSERTION //
        $ins->execute();

        // ICI ON RECUPERE DANS UNE VARIABLE MESSAGE LES CHAMP REMPLI DU FORUMAILRE AVEC UN RETOUR A LA LIGNE. ON UTILISE ENSUITE LA FONCTION MAIL //
        $message = $_POST['nom'] . "\r\n";
        $message .= $_POST['sujet'] . "\r\n";
        $message .= $_POST['message'] . "\r\n";
        $message .= $_POST['mail'] . "\r\n";
        $message .= $_POST['tel'] . "\r\n";
        mail("cedric.bras@gmail.com", "sujet du courrier", $message, "From: HEXAMANUT");
        // UNE FOIS LE SCRIPT EXECUTER , ON LANCE UNE REDIRECTION SUR THANKS.PHP //
        header('Location: thanks.php');

        break;
}
