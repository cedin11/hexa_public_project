<?php
// JE LANCE MA REQUETE SUR LA TABLE ARTICLE EN FONCTION DE L'ID CORRESPONDANT //
$req = $cnx->prepare("SELECT * FROM article WHERE id=?");
// JE RECUPERE L'ID CORRESPONDANT A MON ARTICLE //
$req->execute([$_GET['id']]);
// JE MET MA REQUETE DANS LA VARIABLE DATA //
$data = $req->fetch();

// JE CIBLE MON DOSSIER EN FONCTION DE MON ID ARTICLE AVEC MA VARIABLE DATA //
$dir = "./assets/upload/imgArticles/imgSupplementaires/" . $data['id'];
// ARRAY_SLICE : LA FONCTION PERMET DE SUPPRIMÉ LE . ET LE .. DU TABLEAU SCANDIR //
$a = array_slice(scandir($dir), 2);

// JE ECHO CE DONT J'AI BESOIN D'AFFICHÉ //
echo '<div class="content">';
echo '<div class="container-seearticle">';
echo "<img class='img-seearticle reveal' src='./assets/upload/imgArticles/" . $data['imgname'] . " ' >";
echo '<article class="contenu-seearticle reveal">';
echo $data['contenu'];
echo '</article>';
echo '</div>';
echo '</div>';
echo '<section class="galerie-seearticle">';
// JE CREE UNE BOUCLE FOREACH POUR RECUPERÉ TOUTE LES IMAGES DU DOSSIER CORRESPONDANT A L'ID //
foreach ($a as $img) :
    echo '<div class="wrap-seearticle">';
    echo '<img class="dir-seearticle reveal" src="' . $dir . '/' . $img . '">';
    echo '</div>';
endforeach;
echo '</section>';
