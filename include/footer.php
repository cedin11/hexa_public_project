<footer>
    <section class="section-footer">
        <div class="raccourcis">
            <br>
            <h3 class="h3-raccourcis reveal">Raccourcis</h3><br>
            <a class="reveal" href="#">Qui sommes nous</a><br>
            <!-- POUR CHAQUE LIEN ON UTILISE LA REDIRECTION DANS LE HREF  -->
            <a class="reveal" href="?page=nosPresta">Nos préstations</a><br>
            <a class="reveal" href="?page=nosRea">Nos réalisations</a><br>
            <a class="reveal" href="?page=contact">Contact</a><br>
            <a class="reveal" href="?page=mentions">Mentions Légales</a><br>
        </div>
        <div class="footer-logo">
            <br>
            <h3 class="h3-logo reveal">Hexamanut</h3>
            <img class="logo-footer reveal" src="./assets/image/prest-2.png" alt="hexamanut société manutention BTP">
        </div>
        <div class="contact">
            <br>
            <h3 class="h3-contact reveal">Nous trouver</h3><br>
            <p class="p-contact reveal"><i class="icofont-flag"></i>Adresse :</p>
            <?php
            // ON LANCE LA REQUETE AVEC UNE LIMITE DE 1 POUR AFFICHER LES DERNIERES INFORMATIONS ENREGISTRER PAR L'ENTREPRISE //
            $req = $cnx->query("SELECT * FROM infos ORDER BY id desc limit 1");
            // ON STOCK LA REQUETE DANS UNE VARIABLE DATA //
            $data = $req->fetch();
            // ON AFFICHE LE CONTENU //
            echo '<p class="p-contact reveal">' . $data['adresse'] . '</p><br>';
            echo '<p class="p-contact reveal"><i class="icofont-phone"></i>Telephone :</p>';
            echo '<p class="p-contact reveal">' . $data['tel'] . '</p><br>';
            echo '<p class="p-contact reveal"><i class="icofont-email"></i>E-mail :</p>';
            echo '<p class="p-contact reveal">' . $data['email'] . '</p><br>';
            ?>
        </div>
    </section>
</footer>

</body>

</html>