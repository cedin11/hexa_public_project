<h1 class="h1">Modifier vos informations contact</h1>
<?php
// JE CRÉE MON SWITCH ET J'AFFICHE EN DEFAUT LE FORMULAIRE //
switch ($_GET['action']) {
    default:
?>
        <div class="container-form">
            <form action="index.php?page=infosContact&action=save" method="POST" enctype="multipart/form-data">
                <input class="input-form" placeholder="Adresse" type="text" name="adresse" id="adresse"><br>
                <input class="input-form" placeholder="Téléphone" type="text" name="tel" id="tel"><br>
                <input class="input-form" placeholder="E-mail" type="text" name="email" id="email"><br>
                <div class="container-btn">
                    <button type="submit">Envoyer</button>
                </div>
            </form>
        </div>
<?php
        break;
    case 'save':
        // JE PREPARE L'INSERTION EN BDD //
        $ins = $cnx->prepare("INSERT INTO infos SET adresse=?, tel=?, email=?");
        // J'EXECUTE MA REQUETE //
        $ins->execute([$_POST['adresse'], $_POST['tel'], $_POST['email']]);
        // J'AFFICHE LE MESSAGE //
        echo '<h2 style="width:100% ;text-align:center;">Vos informations contact ont bien été modifiés avec succes!</h2>';
        break;
}
