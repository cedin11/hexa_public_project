<?php
// JE CRÉE MON SWITCH ET J'AFFICHE EN DEFAUT LE FORMULAIRE //
switch ($_GET['action']) {
    default:
?>
        <div class="container-form">
            <form action="?page=mdp&action=change" method="post">
                <label for="login">Modifier votre Identifiant de connexion :</label>
                <input class="input-form" type="text" id="login" name="login">
                <label for="mdp">Nouveau Mot de passe :</label><br>
                <input class="input-form" type="password" class="" id="mdp" name="mdp">
                <div class="container-btn">
                    <button type="submit">
                        Envoyer
                    </button>
                </div>
            </form>
        </div>
<?php
        break;
    case "change":
        // JE CRÉE UNE VARIABLE OU JE STOCK LE MDP //
        $mdp = $_POST["mdp"];
        // JE CRÉE MA REQUETE UPDATE TABLE USER OU L'ID EST EGALE A 1 CAR IL N'Y A QUE 1 SEUL USER DANS LA TABLE //
        $req = "UPDATE user SET mdp= :mdp , login= :login WHERE id = 1 ";
        $upd = $cnx->prepare($req);
        // JE HASH LE PASSWORD AVEC LA FONCTION PASSWORD_HACH //
        $hash = password_hash(
            $mdp,
            PASSWORD_DEFAULT
        );
        // J'UPDATE LE MDP HASHER ET LE LOGIN //
        $upd->bindValue('mdp', $hash);
        $upd->bindValue('login', $_POST['login']);
        // J'EXECUTE MA REQUETE // 
        $upd->execute();
        // J'AFFICHE LE MESSAGE // 
        echo '<h2 style="width:100% ;text-align:center;">Votre mot de passe a bien été modifier avec succès</h2>';
        break;
}
