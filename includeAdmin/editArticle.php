<?php
// J'EXECUTE MA REQUETE EN FONCTION DE L'ID CORRESPONDANT //
$req = $cnx->prepare("SELECT * FROM article WHERE id=?");
$req->execute([$_GET['id']]);
$data = $req->fetch();
// JE CRÉE MON SWITCH AVEC POUR DEFAUT LE FORMULAIRE 
switch ($_GET['action']) {
    default:
?>
        <div class="container-form">
            <!-- DANS LE FORMULAIRE JE RÉCUPERE LES VALEURS DE L'ID CORRESPONDANT POUR LES AFFICHÉS -->
            <form action="index.php?page=editArticle&action=update&id=<?php echo $_GET['id']; ?>" method="post">
                <input class="input-form" placeholder="Nom de l'article" type="text" name="title" id="title" value="<?php echo $data['title']; ?>"><br>
                <input class="input-form" placeholder="Description de l'article" type="text" name="description" id="description" value="<?php echo $data['description']; ?>"><br>
                <textarea name="contenu" id="contenu"><?php echo $data['contenu']; ?></textarea><br>
                <div class="container-btn">
                    <button type="submit">Modifier l'article</button>
                </div>
            </form>
        </div>

<?php
        break;
    case 'update':
        $req = "UPDATE article SET title= :title, description= :description, dateCrea= :dateCrea, contenu= :contenu ";
        $req .= " WHERE id= :id";
        // JE METS A JOUR MA PAGE DANS LA BDD //
        $upd = $cnx->prepare($req);
        // TOUTES LA VARIABLES REQUIRED //
        $upd->bindValue('title', $_POST['title']);
        $upd->bindValue('description', $_POST['description']);
        $upd->bindValue('dateCrea', date("Y-m-d H:i:s"));
        $upd->bindValue('contenu', $_POST['contenu']);
        $upd->bindValue('id', $_GET['id']);
        // J'EXECUTE MA REQUETE UPDATE //
        $upd->execute();
        // J'AFFICHE LE MESSAGE //
        echo '<h2 style="width:100% ;text-align:center;">Votre article a bien été modifié avec succes!</h2>';
        break;
}
?>