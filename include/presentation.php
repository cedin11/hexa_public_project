<?php
// JE LANCE MA REQUETE SUR LA TABLE PRESENTATION ET JE LIMITE A 1 POUR AFFICHÉ LE DERNIER //
$req = $cnx->prepare("SELECT * FROM presentation ORDER BY id desc limit 1");
// JE RECUPERE L'ID CORRESPONDANT A MA PRESENTATION //
$req->execute([$_GET['id']]);
// JE MET MA REQUETE DANS LA VARIABLE DATA //
$data = $req->fetch();

// JE CIBLE MON DOSSIER EN FONCTION DE MON ID ARTICLE AVEC MA VARIABLE DATA //
$dir = "./assets/upload/imgPresentation/imgSupplementaires/" . $data['id'];
// ARRAY_SLICE : LA FONCTION PERMET DE SUPPRIMÉ LE . ET LE .. DU TABLEAU SCANDIR //
$a = array_slice(scandir($dir), 2);

// JE ECHO CE DONT J'AI BESOIN D'AFFICHÉ //
echo '<div class="content">';
echo '<div class="container-seepresentation">';
echo "<img class='img-seepresentation reveal' src='./assets/upload/imgPresentation/" . $data['imgname'] . " ' >";
echo '<article class="contenu-seepresentation reveal">';
echo $data['contenu'];
echo '</article>';
echo '</div>';
echo '</div>';
echo '<section class="galerie-seepresentation">';
// JE CREE UNE BOUCLE FOREACH POUR RECUPERÉ TOUTE LES IMAGES DU DOSSIER CORRESPONDANT A L'ID //
foreach ($a as $img) :
    echo '<div class="wrap-seepresentation">';
    echo '<img class="dir-seepresentation reveal" src="' . $dir . '/' . $img . '">';
    echo '</div>';
endforeach;
echo '</section>';
