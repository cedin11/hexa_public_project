<h1 class="h1nosRea">Découvrez toutes nos réalisations</h1>

<section class="section-nosRea">
  <article class="wrapper article-list">
    <?php
    // ON LANCE LA REQUETE PUIS JE BOUCLE WHILE POUR AFFICHÉ TOUT LES ARTICLES //
    $req = $cnx->query('SELECT * FROM article');
    while ($data = $req->fetch()) {
      echo "<div class='wrapper-2 article'>";
      echo "<h2 class='title-rea reveal'>" . $data['title'] . "</h2>";
      echo "<img class='img-article reveal' src='./assets/upload/imgArticles/" . $data['imgname'] . "' width='300px' >";
      echo "<p class='reveal'>" . $data['dateCrea'] . "</p>";
      echo "<p class='reveal'>" . $data['description'] . "</p>";
      echo '<a class="reveal" href="?page=articles&id=' . $data['id'] . '"">Voir plus +</a>';
      echo "</div>";
    }
    ?>
  </article>
</section>