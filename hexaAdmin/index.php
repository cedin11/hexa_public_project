<!-- ROUTER ZONE ADMIN -->
<?php
// J'INCLUS MON FICHIER TOP.PHP //
require_once('../include/top.php');
?>
<link rel="stylesheet" href="../stylesheetAdmin/main.css">
<link rel="stylesheet" href="../assets/icons/icofont.min.css">
<title>Zone Admin Hexamanut</title>
</head>

<body>
    <?php
    // ON VÉRIFIE LES CONNEXIONS PERMANENTES (COOKIES) //
    if (isset($_COOKIE['login'])) {
        // ON VÉRIFIE QUE LES COOKIES CORRESPONDENT AUX DONNÉES DANS LA TABLE USER //
        $s = $cnx->prepare('SELECT * FROM user WHERE login=? AND mdp=?');
        $s->execute([$_COOKIE['login'], $_COOKIE['mdp']]);
        // SI CE N'EST PAS LE CAS ALORS JE REDIRIGE VERS LOGIN.PHP //
        if ($s->rowCount() == 0) {
            header('Location:login.php');
        }
        // SI OK , ON CRÉE UNE SESSION //
        else {
            $_SESSION['login'] = $_COOKIE['login'];
        }
    }
    // SI NOUS SOMMES CONNECTER, LA VARIABLE DE SESSION A ÉTÉ CRÉE, ON AFFICHE ALORS LA PAGE //
    if (isset($_SESSION['login'])) {
    ?>
        <header class="header">
            <nav>
                <a href="index.php"><img src="../assets/image/logo.png" alt=""></a>
                <ul>
                    <li>
                        <a href="?page=creeArticle">Ajouter un article</a>
                    </li>
                    <li>
                        <a href="?page=creePrestation">Ajouter une prestation</a>
                    </li>
                    <li>
                        <a href="?page=creePresentation">Cree votre page presentation</a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="?page=imgAccueil">Image accueil</a>
                    </li>
                    <li>
                        <a href="?page=infosContact">Infos Contact</a>
                    </li>
                    <li>
                        <a href="?page=messages">Messages reçu</a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="?page=mdp">Modifier mot de passe</a>
                    </li>
                    <li style="list-style: none;">
                        <a class="logout" href="logout.php">Déconnexion</a>
                    </li>
                </ul>
            </nav>
        </header>
        <?php
        // JE CRÉE UN SWITCH AVEC UN CONTENU PAR DEFAUT //
        switch ($_GET['page']) {
            default:
        ?>
                <h2 style="text-align:center;margin-top:50px">Mes Articles</h2>;
                <table class="table" style="margin:auto;">
                    <tr>
                        <th>Titre</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>Visualiser</th>
                        <th>Éditer</th>
                        <th>Supprimer</th>
                    </tr>
                    <?php
                    // J'EXECUTE MA REQUETE POUR RÉCUPERER MES ARTICLES EXISTANT //
                    $req = $cnx->query('SELECT * FROM article');
                    // JE BOUCLE MES ARTICLES PUIS JE LES AFFICHE SOUS FORME D'UN TABLEAU EN HTML //
                    while ($data = $req->fetch()) {
                        echo '<tr>';
                        echo "<td>" . $data['title'] . "</td>";
                        echo "<td>" . $data['description'] . "</td>";
                        echo "<td>" . $data['dateCrea'] . "</td>";
                        echo '<td><a href="?page=visualiserArticle&id=' . $data['id'] . '"><i class="icofont-eye-alt"></i></a></td>';
                        echo '<td><a href="?page=editArticle&id=' . $data['id'] . '"><i class="icofont-edit"></i></a></td>';
                        echo '<td><a onclick="return checkDelete()" href="?page=supArticle&id=' . $data['id'] . '"><i class="icofont-ui-delete"></i></a></td>';
                        echo '</tr>';
                    }
                    ?>
                </table>

                <hr style="margin-top: 50px;">



                <h2 style="text-align:center;margin-top:50px">Mes Prestations</h2>
                <table class="table" style="margin:auto;">
                    <tr>
                        <th>Titre</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>Visualiser</th>
                        <th>Éditer</th>
                        <th>Supprimer</th>
                    </tr>
                    <?php
                    // J'EXECUTE MA REQUETE POUR RÉCUPERER MES PRESTATIONS EXISTANT //
                    $req = $cnx->query('SELECT * FROM prestation');
                    // JE BOUCLE MES PRESTATIONS PUIS JE LES AFFICHE SOUS FORME D'UN TABLEAU EN HTML //
                    while ($data = $req->fetch()) {
                        echo '<tr>';
                        echo "<td>" . $data['title'] . "</td>";
                        echo "<td>" . $data['description'] . "</td>";
                        echo "<td>" . $data['dateCrea'] . "</td>";
                        echo '<td><a href="?page=visualiserPrestation&id=' . $data['id'] . '"><i class="icofont-eye-alt"></i></a></td>';
                        echo '<td><a href="?page=editPrestation&id=' . $data['id'] . '"><i class="icofont-edit"></i></a></td>';
                        echo '<td><a onclick="return checkDelete()" href="?page=supPrestation&id=' . $data['id'] . '"><i class="icofont-ui-delete"></i></a></td>';
                        echo '</tr>';
                    }
                    ?>
                </table>
    <?php
                break;
            case 'creeArticle':
                require_once('../includeAdmin/creeArticle.php');
                break;
            case 'creePrestation':
                require_once('../includeAdmin/creePrestation.php');
                break;
            case 'creePresentation':
                require_once('../includeAdmin/creePresentation.php');
                break;
            case 'supArticle':
                require_once('../includeAdmin/supArticle.php');
                break;
            case 'editArticle':
                require_once('../includeAdmin/editArticle.php');
                break;
            case 'visualiserArticle':
                require_once('../includeAdmin/visualiserArticle.php');
                break;
            case 'supPrestation':
                require_once('../includeAdmin/supPrestation.php');
                break;
            case 'editPrestation':
                require_once('../includeAdmin/editPrestation.php');
                break;
            case 'visualiserPrestation':
                require_once('../includeAdmin/visualiserPrestation.php');
                break;
            case 'imgAccueil':
                require_once('../includeAdmin/imageaccueil.php');
                break;
            case 'infosContact':
                require_once('../includeAdmin/infosContact.php');
                break;
            case 'messages':
                require_once('../includeAdmin/messages.php');
                break;
            case 'mdp':
                require_once('../includeAdmin/mdp.php');
                break;
        }
    } else {
        // SI ON EST PAS CONNECTER , ON RENVOIE A LA PAGE LOGIN.PHP //
        header('Location:login.php');
    }
    ?>
    <!-- ICI J'E VAIS CHERCHER LES SCRIPT POUR POUVOIR UTILISER CKEDITOR -->
    <script src="../js/ckeditor.js"></script>
    <script>
        function checkDelete() {
            return confirm('Confirmer la suppression ?');
        }
    </script>
    <script>
        const watchdog = new CKSource.EditorWatchdog();

        window.watchdog = watchdog;

        watchdog.setCreator((element, config) => {
            return CKSource.Editor
                .create(element, config)
                .then(editor => {




                    return editor;
                })
        });

        watchdog.setDestructor(editor => {



            return editor.destroy();
        });

        watchdog.on('error', handleError);

        watchdog
            .create(document.querySelector('#contenu'), {

                licenseKey: '',



            })
            .catch(handleError);

        function handleError(error) {
            console.error('Oops, something went wrong!');
            console.error('Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:');
            console.warn('Build id: 4trydeqg494i-ddvpluz0nxun');
            console.error(error);
        }
    </script>