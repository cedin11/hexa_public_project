-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : lun. 22 août 2022 à 12:45
-- Version du serveur : 5.7.34
-- Version de PHP : 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `hexamanut`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCrea` date NOT NULL,
  `contenu` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `title`, `imgname`, `description`, `dateCrea`, `contenu`) VALUES
(40, 'test IMG', '62d035eb84a367.28797778.png', 'Test IMG', '2022-07-14', '<h1>TEST IMG</h1><p>&nbsp;</p><p><span style=\"color:#9cdcfe;\">required</span><span style=\"color:#d4d4d4;\">=</span><span style=\"color:#ce9178;\">\"required\"</span></p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p>&nbsp;</p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p>&nbsp;</p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">required=\"required\"required=\"required\"</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">v</span></p><p><span style=\"color:#ce9178;\">required=\"required\"v</span></p><p>&nbsp;</p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">required=\"required\"</span></p><p><span style=\"color:#ce9178;\">v</span></p>'),
(41, 'TEST IMG 2', '62d0368c8d6ce2.76131831.jpeg', 'TEST IMG 2', '2022-07-14', '<h1>TEST IMG 2</h1><p>&nbsp;</p>'),
(42, 'test', '62d1f2bbbe1105.84299871.jpeg', 'test', '2022-07-15', '<h1>eee</h1><p>eee</p>'),
(45, 'Test img', '62d1f5300c8a91.70597641.webp', 'Test img', '2022-07-15', '<h1>Test img</h1><p>Test img</p><p>Test img</p><p>Test img</p><p>Test img</p>'),
(46, 'TTTT', '62d1f5b6c51981.65216603.webp', 'TTTT', '2022-07-15', '<h1>TTTT</h1><p>TTTT</p>'),
(50, 'BBBBBB', '62d1f8ca5678f2.56547199.webp', 'BBBBBB', '2022-07-15', '<h1>BBBBBB</h1><p>BBBBBB</p>'),
(51, 'AAAAAA', '62d25d6e09c287.32253246.webp', 'AAAAAA', '2022-07-16', '<h1>AAAAAA</h1><p>AAAAAA</p><p>v</p><p>&nbsp;</p><p>v</p><p>v</p><p>vAAAAAA</p>'),
(52, 'AAAAAA', '62d25d8c84abb5.94134329.webp', 'AAAAAA', '2022-07-16', '<h1>AAAAAA</h1><p>AAAAAA</p><p>v</p><p>&nbsp;</p><p>v</p><p>v</p><p>vAAAAAA</p>'),
(53, 'AAAAAA', '62d25ec3517f20.33812123.webp', 'AAAAAA', '2022-07-16', '<h1>AAAAAA</h1><p>AAAAAA</p><p>v</p><p>&nbsp;</p><p>v</p><p>v</p><p>vAAAAAA</p>');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sujet` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` mediumtext COLLATE utf8mb4_unicode_ci,
  `mail` mediumtext COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `tel` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `nom`, `sujet`, `message`, `mail`, `date`, `tel`) VALUES
(1, 'ingrid fevrier', 'sujet test', 'zedzed', 'test@test.com', '2022-07-01', '0664316959'),
(2, 'ingrid fevrier', 'sujet test', 'dzasdazdazd', 'tralala@tralala.fr', '2022-07-01', '0664316959'),
(3, '', 'sujet ', 'ma demande', 'tralala@tralala.fr', '2022-07-09', 'hello test'),
(4, 'zefaef', 'aefaef', 'aefae', 'tralala@tralala.fr', '2022-07-09', 'eaz'),
(5, 'erezrg', 'erg', 'ergerg', 'tralala@tralala.fr', '2022-07-09', 'gergerg'),
(6, 'ingrid fevrier', 'sujet test', 'alkhd', 'tralala@tralala.fr', '2022-07-15', '0664316959');

-- --------------------------------------------------------

--
-- Structure de la table `imgaccueil`
--

CREATE TABLE `imgaccueil` (
  `id` int(11) NOT NULL,
  `imgname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `imgaccueil`
--

INSERT INTO `imgaccueil` (`id`, `imgname`) VALUES
(1, '6278c68b8d1bf5.60612739.jpeg'),
(2, '6278d833731f98.74551180.jpeg'),
(3, '627ac71b5fa207.79894005.jpeg'),
(4, '6290c6a7bbbb21.31858732.jpeg'),
(5, '6290c6bdc24321.34605243.jpeg'),
(6, '62c92e6edd00e6.45330172.jpeg'),
(7, '62d1f95e14e400.53301713.webp');

-- --------------------------------------------------------

--
-- Structure de la table `infos`
--

CREATE TABLE `infos` (
  `id` int(11) NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `infos`
--

INSERT INTO `infos` (`id`, `adresse`, `tel`, `email`) VALUES
(2, '27 le clos de neyve 38200 Serpaize', '0606060606', 'cedric.bras@gmail.com'),
(3, '11 rue des peuplier ', '0909090909', 'test@test.test'),
(4, '00 rue des pourpre', '0102030405', 'test2@test.test'),
(5, '27 le clos de neyve', '0707070707', 'cedric.bras@gmail.com'),
(6, '22 rue des peuplier 69000 Lyon', '0909090909', 'test@test.test');

-- --------------------------------------------------------

--
-- Structure de la table `presentation`
--

CREATE TABLE `presentation` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenu` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `presentation`
--

INSERT INTO `presentation` (`id`, `title`, `imgname`, `description`, `contenu`) VALUES
(1, 'Ma page presentation', '62bc372e8e68d5.79138791.jpeg', 'Ma description', '<h1>Presentation</h1><p>Voici la page presentation&nbsp;</p><p>&nbsp;</p><p>bienvenue&nbsp;</p><p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Structure de la table `prestation`
--

CREATE TABLE `prestation` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCrea` date NOT NULL,
  `contenu` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `prestation`
--

INSERT INTO `prestation` (`id`, `title`, `imgname`, `description`, `dateCrea`, `contenu`) VALUES
(6, 'Ma presta 2', '62bc3a748650f7.87384718.jpeg', 'Ma presta 2', '2022-06-29', '<h1>Ma presta 2</h1><p>&nbsp;</p><p>test de ma presta 2</p>'),
(7, 'Ma presta 3', '62bc3a98722bb8.48211660.jpeg', 'Ma presta 3', '2022-06-29', '<h1>Ma presta 3</h1><p>&nbsp;</p><p>test de ma presta 3</p>'),
(8, 'Ma presta 1', '62cd1aa139ff55.19051552.jpeg', 'Ma presta 1', '2022-07-12', '<h1>Ma presta 1 modifier</h1><p><span style=\"color:#d4d4d4;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae eaque i</span></p><p><span style=\"color:#d4d4d4;\">ncidunt ab praesentium libero quibusdam? Architecto natus reiciendis, nu</span></p><p><span style=\"color:#d4d4d4;\">mquam libero doloremque eveniet delectus autem a voluptates exercitationem, sit veniam alias!</span></p><p>&nbsp;</p><p><span style=\"color:#d4d4d4;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae eaque i</span></p><p><span style=\"color:#d4d4d4;\">ncidunt ab praesentium libero quibusdam? Architecto natus reiciendis, nu</span></p><p><span style=\"color:#d4d4d4;\">mquam libero doloremque eveniet delectus autem a voluptates exercitationem, sit veniam alias!</span></p><p><span style=\"color:#d4d4d4;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae eaque i</span></p><p><span style=\"color:#d4d4d4;\">ncidunt ab praesentium libero quibusdam? Architecto natus reiciendis, nu</span></p><p><span style=\"color:#d4d4d4;\">mquam libero doloremque eveniet delectus autem a voluptat</span></p><ul><li><span style=\"color:#d4d4d4;\">es exercitationem, sit veniam alias!</span></li></ul><p><span style=\"color:#d4d4d4;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae eaque i</span></p><p><span style=\"color:#d4d4d4;\">ncidunt ab praesentium libero quibusdam? Architecto natus reiciendis, nu</span></p><p><span style=\"color:#d4d4d4;\">mquam libero doloremque eveniet delectus autem a voluptates exercitationem, sit veniam alias!</span></p><p><span style=\"color:#d4d4d4;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae eaque i</span></p><p><span style=\"color:#d4d4d4;\">ncidunt ab praesentium libero quibusdam? Architecto natus reiciendis, nu</span></p><p><span style=\"color:#d4d4d4;\">mquam libero doloremque eveniet delectus autem a voluptates exercitationem, sit veniam alias!</span></p><p>&nbsp;</p><p><span style=\"color:#d4d4d4;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae eaque i</span></p><p><span style=\"color:#d4d4d4;\">ncidunt ab praesentium libero quibusdam? Architecto natus reiciendis, nu</span></p><ul><li><span style=\"color:#d4d4d4;\">mquam libero doloremque eveniet delectus autem a voluptates exercitationem, sit veniam alias!</span></li></ul>');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mdp` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCrea` datetime NOT NULL,
  `dateModif` datetime NOT NULL,
  `email` mediumtext COLLATE utf8mb4_unicode_ci,
  `niveau` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `login`, `mdp`, `dateCrea`, `dateModif`, `email`, `niveau`) VALUES
(1, 'hexamanut', '$2y$10$z8Ae0zX.mJtLf0SCf1KsBOV.atMsIxLbWjlMN0zKgSShuUtehEdkS', '2022-04-22 12:56:21', '2022-04-22 12:56:21', NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `imgaccueil`
--
ALTER TABLE `imgaccueil`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `infos`
--
ALTER TABLE `infos`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `presentation`
--
ALTER TABLE `presentation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `prestation`
--
ALTER TABLE `prestation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `imgaccueil`
--
ALTER TABLE `imgaccueil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `infos`
--
ALTER TABLE `infos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `presentation`
--
ALTER TABLE `presentation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `prestation`
--
ALTER TABLE `prestation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
