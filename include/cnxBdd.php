<?php
// JE STOCK DANS DES VARIABLE MES IDENTIFIANT DE CONNEXION A LA BDD (PLUS SIMPLE POUR LES CHANGEMENT LORS DE LA MISE EN LIGNE) //
$bddType = 'mysql';
$bddHost = 'localhost';
$bddName = 'hexamanut';
$bddUser = 'root';
$bddPsw = 'root';

// JE LANCE UN TRY AND CATCH (S'IL NE SE CONNECTE PAS A LA BDD, LE SITE NE SE LANCERA PAS) //
try {
    $cnx = new PDO($bddType . ":host=$bddHost;dbname=$bddName", $bddUser, $bddPsw);
} catch (PDOException $e) {
    echo 'Erreur de connexion :' . $e->getMessage();
}
