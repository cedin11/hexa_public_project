<header id="navbar">
    <nav>
        <a class="logo" href="index.php"><img src="./assets/image/logo.png" alt="logo entreprise hexamanut société manutention BTP"></a>
        <div style="display:none ;" class="spaceleft"></div>
        <li>
            <a class="marginLink" href="?page=presentation">Qui sommes nous</a>
        </li>
        <li class="test">
            <a href="?page=nosPresta">Nos préstations ▾</a>
            <ul class="sous-menu">
                <?php
                // JE LANCE UNE REQUETE POUR RECUPERER LES TITRES DES PRESTATION ET LES AFFICHÉS DANS LE MENU DEROULANT //
                $req = $cnx->query("SELECT * FROM prestation ORDER BY id asc");
                // JE BOUCLE POUR AFFICHÉ LES TITRES //
                while ($data = $req->fetch()) {
                    echo '<li class="liste-presta">';
                    // ON TRANSMET L'ID DANS LE HREF //
                    echo '<a href="?page=prestations&id=' . $data['id'] . '">' . $data['title'] . '</a>';
                    echo '</li>';
                }
                ?>
            </ul>
        </li>
        <li>
            <a href="?page=nosRea">Nos réalisations</a>
        </li>
        <li>
            <a class="marginLinkright" href="?page=contact">Contact</a>
        </li>
    </nav>
    <div>
        <a id="burger" href="#navbar"><i class="icofont-navigation-menu"></i></a>
        <a id="close" href="#"><i class="icofont-close-line"></i></a>
    </div>
</header>

<nav class="reseaux">
    <a href=""><i class="icofont-instagram"></i></a>
    <a href=""><i class="icofont-facebook"></i></a>
    <a href="https://www.linkedin.com/in/hexamanut-la-solution-manutention-48a036140/"><i class="icofont-linkedin"></i></a>
</nav>