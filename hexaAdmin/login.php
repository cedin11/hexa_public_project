<?php require_once('../include/top.php'); ?>
<link rel="stylesheet" href="../stylesheetAdmin/login.css">
<title>Login</title>
</head>

<body>

    <?php
    // ON CRÉÉ UNE CONDITION POUR SE CONNECTER //
    switch ($_GET['action']) {
            // JE CRÉE UN SWITCH AVEC POUR DEFAUT MON FORULAIRE DE CONNEXION //
        default:
    ?>
            <div class="conatinerimg">
                <img src="../assets/image/logo.png" alt="">
            </div>
            <div class="containerLogin">
                <form class="formLogin" action="login.php?action=verif" method="post">
                    <h2 class="h2login">Connexion</h2>
                    <div class="user-box">
                        <input type="text" id="login" name="login" required placeholder="Login">
                    </div><br>
                    <div class="user-box">
                        <input type="password" id="mdp" name="mdp" required placeholder="Mot de passe">
                    </div><br>
                    <hr>
                    <label class="labelConnect" for="cookie">
                        <input type="checkbox" name="cookie" id="cookie" value="1">
                        Rester connecté</label>
                    <input type="submit" class="btnLogin" value="Se connecter">
                </form>
            </div>
    <?php
            break;
        case 'verif':
            //J'EXECUTE MA REQUETE DE VERIFICATION LOGIN //
            $s = $cnx->prepare('SELECT * FROM user WHERE login=?');
            $s->execute([$_POST['login']]);
            // SI LE LOGIN N'EST PAS BON , ON VERIFIE EN COMPTANT LE NOMBRE DE RESULTAT //
            if ($s->rowCount() == 0) {
                echo 'Identifiant incorrect';
            } else {
                // SI LE LOGIN EST BON //
                $r = $s->fetch();
                // ON VERIFIE LE MDP CRYPTER DANS LA BDD AVEC PASSWORD_VERIFY //
                if (password_verify($_POST['mdp'], $r['mdp'])) {
                    // SI LA CASE RESTER CONNECTER A ÉTÉ COCHER //
                    if ($_POST['cookie'] == "1") {
                        // JE CRÉÉ MES 2 COOKIES VALABLE 1 HEURE //
                        setcookie("login", $r['login'], time() + 3600);
                        setcookie("mdp", $r['mdp'], time() + 3600);
                    }
                    // SI TOUT EST OK ALORS ON CRÉE LA VARIABLE DE SESSION //
                    $_SESSION['login'] = $r['login'];
                    //  PUIS ON REDIRIGE VERS LA PAGE INDEX.PHP //
                    header('Location:index.php');
                } else {
                    //SINON J'AFFICHE LE MESSAGE ERREUR //
                    echo "Erreur mot de passe";
                }
                break;
            }
    }
    ?>