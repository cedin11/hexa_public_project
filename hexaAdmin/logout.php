<?php
// ON DEMARRE LA SESSION //
session_start();
// ON SUPPRIME LA VARIABLE DE SESSION //
unset($_SESSION['login']);
setcookie("login");
setcookie("mdp");
// ON FAIT UNE REDIRECTION VERS LA CONNEXION LOGIN.PHP //
header('Location:login.php');
