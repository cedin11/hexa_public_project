<h1 class="h1">Modifier votre image d'accueil</h1>
<?php
// JE CRÉE MON SWITCH AVEC POUR DEFAUT MON FORMULAIRE //
switch ($_GET['action']) {
    default:
?>
        <div class="container-form">
            <form action="index.php?page=imgAccueil&action=save" method="POST" enctype="multipart/form-data">
                <label for="file">Choisiser votre image :</label><br>
                <input type="file" name="file"><br>
                <div class="container-btn">
                    <button type="submit">Envoyer</button>
                </div>
            </form>
        </div>
<?php
        break;

    case 'save':
        if (isset($_FILES['file'])) {
            // JE RECUPERE DANS DES VARIABLES LES VALEURS DE MON IMAGE PRINCIPAL //
            $tmpName = $_FILES['file']['tmp_name'];
            $name = $_FILES['file']['name'];
            $size = $_FILES['file']['size'];
            $error = $_FILES['file']['error'];
            // JE SÉPARE LE NOM DE L'EXTENSION //
            $tabExtension = explode('.', $name);
            // JE PASSE LE TOUT EN MINUSCULE //
            $extension = strtolower(end($tabExtension));
            // JE CRÉÉ UNE VARIABLE AVEC UNE TAILLE MAX POUR LES IMAGES //
            $maxSize = 400000;
            // VARIABLES AVEC TABLEAU DES EXTENSIONS AUTORISER //
            $extensions = ['jpg', 'png', 'jpeg', 'webp'];
            // JE CRÉE UNE CONDITION QUI VERIFIE LES EXTENSIONS ET LA TAILLE MAX DES IMAGES //
            if (in_array($extension, $extensions) && $size <= $maxSize && $error == 0) {
                // JE GENERE UN UNOIQUE ID QUI RESSEMBLERA A CA : 5f586bf96dcd38.73540086 //
                $uniqueName = uniqid('', true);
                // PUIS JE CONCQUATENE AVEC L'EXTENSION //
                //$file = 5f586bf96dcd38.73540086.WEBP //
                $file = $uniqueName . "." . 'webp';
                // J'ENVOIE MON IMAGE DANS LE DOSSIER AVEC  move_uploaded_file //
                move_uploaded_file($tmpName, '../assets/upload/' . $file);
                // J'EXECUTE MA REQUETE POUR INSERTIONS //
                $ins = $cnx->prepare("INSERT INTO imgaccueil SET imgname=?");
                $ins->execute([$file]);
            } else {
                // SI ERREUR J'AFFICHE //
                echo '<h2 style="width:100% ;text-align:center;">Une erreur est survenue</h2>';
            }
        }
        // SI OK J'AFFICHE //
        echo '<h2 style="width:100% ;text-align:center;">Image enregistrée</h2>';
        break;
}
