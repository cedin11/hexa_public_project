<section class="section-mentions">
    <div class="container-mentions">
        <h1 class="h1-mentions">Mentions Légales</h1>
        <p class="p-mentions p-firstMentions">Site édité par Cédric Bras</p><br>
        <p class="p-mentions">Site hébergé par OVH</p><br>
        <p class="p-mentions">Ce site n'utilise pas les cookies</p><br>
        <p class="p-mentions">Votre mail est collecté dans le seul but de répondre à une demande transmise par la rubrique contact. Il n'est en aucun cas transmis à un tiers sans votre consentement.</p><br>
        <div class="container-btn"><a class="mentions-btn" href="index.php">Retour</a></div>
    </div>
</section>