<!-- PAGE ROUTEUR PARTIE FRONT -->
<?php
// J'INCLUE MON TOP.PHP //
require_once('include/top.php'); ?>

<body>
    <?php
    // J'INCLUE MON HEADER.PHP //
    require_once('include/header.php');
    // JE CREE MON SWITCH AVEC UN RENDU PAR DEFAUT //
    switch ($_GET['page']) {
        default:
    ?>
            <!--     PARTIE ACCUEIL FIGURE ET FIGCAPTION -->
            <figure>
                <div class="colororange"></div>
                <figcaption>
                    <div class="img-accueil">
                        <div class="divdeco"></div>
                        <?php
                        // J'EXECUTE MA REQUETE TABLE IMGACCUEIL POUR RECUPERER LA DERNIERE IMAGE ENREGISTRER //
                        $req = $cnx->query("SELECT imgname FROM imgaccueil ORDER BY id desc limit 1");
                        $data = $req->fetch();
                        // J'AFFICHE MON IMAGE EN FRONT //
                        echo "<img src='./assets/upload/" . $data['imgname'] . " 'alt='image accueil  hexamanut société manutention BTP' >";
                        ?>
                    </div>
                    <div class="text-accueil">
                        <img class="logo-phone" src="./assets/image/logo.png" alt="Logo Hexamanut">
                        <h1 class="texthaut reveal">La<br> qualité,</h1>
                        <h1 class="textbas reveal">notre<br> priorité !</h1>
                    </div>
                </figcaption>
            </figure>
            <!--   PARTIE VALEURS -->
            <section class="section-valeurs">
                <article>
                    <h3 class="reveal">Zéro risque</h3>
                    <p class="reveal">Les métiers de la manutention sont des professions possédant
                        de nombreux facteurs de risques et/ou d'accidents pour le manutentionnaire.</p>
                    <p class="reveal">En passant par HexaManut, vous externalisez ces risques.</p>
                    <p class="reveal"> Vos travailleurs ne
                        verront pas leur santé dégradée par ces lourds efforts et
                        votre taux d'accident du travail diminuera.</p>
                </article>
                <aside class="reveal">
                    <img src="./assets/image/no-risk.jpg" alt="hexamanut société manutention BTP">
                </aside>
                <aside class="reveal desk">
                    <img src="./assets/image/flexibility.jpg" alt="hexamanut société manutention BTP">
                </aside>
                <article class="desk">
                    <h3 class="reveal">Fléxibilité, tranquillité</h3>
                    <p class="reveal">En passant par HexaManut pour vos travaux de manutention,</p>
                    <p class="reveal">
                        vous n'avez plus aucun souci lié à la planification, à l'attribution
                        des ressources humaines et du matériel,</p>
                    <p class="reveal"> à la gestion des arrivages sur
                        chantier, aux contacts avec les transporteurs...</p>
                </article>
                <article class="mobile">
                    <h3 class="reveal">Fléxibilité, tranquillité</h3>
                    <p class="reveal">En passant par HexaManut pour vos travaux de manutention,</p>
                    <p class="reveal">
                        vous n'avez plus aucun souci lié à la planification, à l'attribution
                        des ressources humaines et du matériel,</p>
                    <p class="reveal"> à la gestion des arrivages sur
                        chantier, aux contacts avec les transporteurs...</p>
                </article>
                <aside class="reveal mobile">
                    <img src="./assets/image/flexibility.jpg" alt="hexamanut société manutention BTP">
                </aside>
                <article>
                    <h3 class="reveal">Performance</h3>
                    <p class="reveal">HexaManut met à votre disposition des ouvriers qualifiés
                        et expérimentés aux métiers de la manutention sur chantier
                        vous apportant ainsi un gain de temps. </p>
                    <p class="reveal"> Vous pourrez
                        vous concentrer sur votre coeur de métier sans vous soucier des tâches
                        liées à la manutention.</p>
                </article>
                <aside class="reveal">
                    <img src="./assets/image/performance.jpg" alt="hexamanut société manutention BTP">
                </aside>
            </section>
            <!-- PARTIE ACTUALITÉS -->
            <h2 class="actu-title reveal">NOS DERNIÈRES ACTUALITÉS</h2>
            <main class="actu">
                <aside>
                    <?php
                    // J'EXECUTE MA REQUETE ET JE DEFINI UNE LIMITE DE 3 //
                    $req = $cnx->query("SELECT * FROM article ORDER BY id desc limit 3");
                    // JE LANCE MA REQUETE DANS UNE BOUCLE WHILE ET JE ECHO CE DONT J'AI BESOIN//
                    while ($data = $req->fetch()) {
                        echo '<article>';
                        echo "<img class='img-card' src='./assets/upload/imgArticles/" . $data['imgname'] . " ' alt='hexamanut société manutention BTP'><br>";
                        echo '<p class="dateCrea">' . $data['dateCrea'] . '</p><br>';
                        echo '<p class="title-card">' . $data['title'] . '</p>';
                        echo '<a class="link-card" href="?page=articles&id=' . $data['id'] . '">En savoir +</a>';
                        echo '</article>';
                    }
                    ?>
                </aside>
            </main>

    <?php
            break;
            // ICI J'INCLUDE LES PAGE EN FONCTION DES CASE //
        case 'nosRea':
            require_once('include/nosRea.php');
            break;
        case 'nosPresta':
            require_once('include/nosPresta.php');
            break;
        case 'articles':
            require_once('include/articles.php');
            break;
        case 'prestations':
            require_once('include/prestations.php');
            break;
        case 'presentation':
            require_once('include/presentation.php');
            break;
        case 'contact':
            require_once('include/contact.php');
            break;
        case 'mentions':
            require_once('include/mentions.php');
            break;
    }
    ?>

    <?php
    // INCLUDE DU FOOTER //
    include('include/footer.php');
    ?>
    <script src="./js/script.js"></script>
</body>

</html>