<?php
// JE LANCE MA REQUETE SUR LA TABLE PRESTATION EN FONCTION DE L'ID CORRESPONDANT //
$req = $cnx->prepare("SELECT * FROM prestation WHERE id=?");
// JE RECUPERE L'ID CORRESPONDANT A MA PRESTATION //
$req->execute([$_GET['id']]);
// JE MET MA REQUETE DANS LA VARIABLE DATA //
$data = $req->fetch();

// JE CIBLE MON DOSSIER EN FONCTION DE MON ID ARTICLE AVEC MA VARIABLE DATA //
$dir = "./assets/upload/imgPrestations/imgSupplementaires/" . $data['id'];
// ARRAY_SLICE : LA FONCTION PERMET DE SUPPRIMÉ LE . ET LE .. DU TABLEAU SCANDIR //
$a = array_slice(scandir($dir), 2);

// JE ECHO CE DONT J'AI BESOIN D'AFFICHÉ //
echo '<div class="content">';
echo '<div class="container-seepresta">';
echo "<img class='img-seepresta reveal' src='./assets/upload/imgPrestations/" . $data['imgname'] . " ' >";
echo '<article class="contenu-seepresta reveal">';
echo $data['contenu'];
echo '</article>';
echo '</div>';
echo '</div>';
echo '<section class="galerie-seepresta">';
// JE CREE UNE BOUCLE FOREACH POUR RECUPERÉ TOUTE LES IMAGES DU DOSSIER CORRESPONDANT A L'ID //
foreach ($a as $img) :
    echo '<div class="wrap-seepresta">';
    echo '<img class="dir-seepresta reveal" src="' . $dir . '/' . $img . '">';
    echo '</div>';
endforeach;
echo '</section>';
