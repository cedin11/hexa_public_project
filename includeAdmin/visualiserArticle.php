<?php
// JE CRÉE MA REQUETE TABLE ARTICLE EN FONCTION DE L'ID CORRESPONDANT //
$req = $cnx->prepare("SELECT * FROM article WHERE id=?");
$req->execute([$_GET['id']]);
$data = $req->fetch();

// JE CIBLE MON DOSSIER EN FONCTION DE MON ID ARTICLE AVEC MA VARIABLE $DATA //
$dir = "../assets/upload/imgArticles/imgSupplementaires/" . $data['id'];
// ARRAY_SLICE :  PERMET DE SUPPRIMÉ LE . ET LA .. DU TABLEAU SCANDIR //
$a = array_slice(scandir($dir), 2);
// J'AFFICHE LE CONTENU SOUHAITER //
echo '<div class="content">';
echo '<div class="container-seearticle">';
echo "<img class='img-seearticle' src='../assets/upload/imgArticles/" . $data['imgname'] . " ' >";
echo '<article class="contenu-seearticle">';
echo $data['contenu'];
echo '</article>';
echo '</div>';
echo '</div>';
echo '<section class="galerie-seearticle">';
// ICI JE BOUCLE MES IMAGES DU DOSSIER SELECTIONNÉ //
foreach ($a as $img) :
    echo '<div class="wrap-seearticle">';
    echo '<img class="dir-seearticle" src="' . $dir . '/' . $img . '">';
    echo '</div>';
endforeach;
echo '</section>';
